package SementTree;

public class SegmentTree<E> {

    // E[] tree, E[] data, Merger<E> merger
    private E[] tree;
    private E[] data;
    private Merger<E> merger;

    // SegmentTree(E[] arr, Merger<E> merger)
    public SegmentTree(E[] arr, Merger<E> merger){
        if (arr == null || arr.length == 0){
            throw new IllegalArgumentException("The arr is null or empty.");
        }

        this.data = (E[])new Object[arr.length];
        this.tree = (E[])new Object[4 * arr.length];
        this.merger = merger;

        for (int i = 0;i < arr.length;i ++)
            data[i] = arr[i];

        buildSegmentTree(0, 0, data.length - 1);
    }

    // 在treeIndex的位置创建表示区间[l...r]的线段树 buildSegmentTree(int treeIndex, int l, int r)
    private void buildSegmentTree(int treeIndex, int l, int r){
        if (l == r) {
            tree[treeIndex] = data[l];
            return;
        }

        int leftChild = leftChild(treeIndex);
        int rightChild = rightChild(treeIndex);
        int mid = l + (r - l)/2;

        buildSegmentTree(leftChild, l, mid);
        buildSegmentTree(rightChild, mid+ 1, r);

        tree[treeIndex] = merger.merge(tree[leftChild], tree[rightChild]);

    }

    // int getSize()
    public int getSize(){
        return data.length;
    }

    // E get(int index)
    private E get(int index){
        if (index < 0 || index >= getSize())
            throw new IllegalArgumentException("The index is illegal.");

        return data[index];
    }

    // 返回完全二叉树的数组表示中，一个索引所表示的元素的左孩子节点的索引 int leftChild(int index)
    private int leftChild(int index){
        if (index < 0 || index >= getSize())
            throw new IllegalArgumentException("The index is illegal.");

        return 2 * index + 1;
    }

    // 返回完全二叉树的数组表示中，一个索引所表示的元素的右孩子节点的索引 int rightChild(int index)
    private int rightChild(int index){
        if (index < 0 || index >= getSize())
            throw new IllegalArgumentException("The index is illegal.");

        return 2 * index + 2;
    }

    // 返回区间[queryL, queryR]的值 E query(int queryL, int queryR)
    public E query(int queryL, int queryR){
        if (queryL < 0 || queryL >= getSize() ||
            queryR < 0 || queryR >= getSize() || queryL > queryR)
            throw new IllegalArgumentException("The queryL or queryR is illegal.");

        return query(0, 0, getSize() - 1, queryL, queryR);
    }

    // 在以treeIndex为根的线段树中[l...r]的范围里，搜索区间[queryL...queryR]的值
    // E query(int treeIndex, int l, int r, int queryL, int queryR)
    private E query(int treeIndex, int l, int r, int queryL, int queryR){
        if (l == queryL && r == queryR)
            return tree[treeIndex];

        int leftChild = leftChild(treeIndex);
        int rightChild = rightChild(treeIndex);
        int mid = l + (r - l)/2;

        if (queryL > mid)
            return query(rightChild, mid + 1, r, queryL, queryR);
        else if (queryR <= mid)
            return query(leftChild, l, mid, queryL, queryR);

        E leftTree = query(leftChild, l, mid, queryL, mid);
        E rightTree = query(rightChild, mid + 1, r, mid + 1, queryR);
        return merger.merge(leftTree, rightTree);
    }


    // 将index位置的值，更新为e void set(int index, E e)
    public void set(int index, E e){
        if (index < 0 || index >= getSize())
            throw new IllegalArgumentException("The index is illegal.");

        data[index] = e;
        set(0, 0, getSize() - 1, index, e);
    }

    // 在以treeIndex为根的线段树中更新index的值为e
    // void set(int treeIndex, int l, int r, int index, E e)
    private void set(int treeIndex, int l, int r, int index, E e){
        if (l == r){
            tree[treeIndex] = e;
            return;
        }

        int leftChild = leftChild(treeIndex);
        int rightChild = rightChild(treeIndex);
        int mid = l + (r - l)/2;

        if (index > mid)
            set(rightChild, mid + 1, r, index, e);
        else
            set(leftChild, l, mid, index, e);

        tree[treeIndex] = merger.merge(tree[leftChild], tree[rightChild]);
    }

    // toString()
    @Override
    public String toString() {
        StringBuilder res = new StringBuilder();
        res.append('[');
        for(int i = 0 ; i < tree.length ; i ++){
            if(tree[i] != null)
                res.append(tree[i]);
            else
                res.append("null");

            if(i != tree.length - 1)
                res.append(", ");
        }
        res.append(']');
        return res.toString();
    }



    
    /*public static void main(String[] args) {

        Integer[] nums = {-2, 0, 3, -5, 2, -1};
//        SegmentTree<Integer> segTree = new SegmentTree<>(nums,
//                new Merger<Integer>() {
//                    @Override
//                    public Integer merge(Integer a, Integer b) {
//                        return a + b;
//                    }
//                });

        SegmentTree<Integer> segTree = new SegmentTree<>(nums,
                (a, b) -> a + b);
        System.out.println(segTree);*/

    public static void main(String[] args) {

        Integer[] nums = {-2, 0, 3, -5, 2, -1};

        SegmentTree<Integer> segTree = new SegmentTree<>(nums,
                (a, b) -> a + b);
        System.out.println(segTree);

        System.out.println(segTree.query(0, 2));
        System.out.println(segTree.query(2, 5));
        System.out.println(segTree.query(0, 5));
    }

}
