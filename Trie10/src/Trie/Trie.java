//package Trie;
//
//import java.util.TreeMap;
//
//public class Trie {
//
//    // private class Node (isWord, next)
//    private class Node{
//        private boolean isWord;
//        private TreeMap<Character, Node> next;
//
//        public Node(boolean isWord){
//            this.isWord = isWord;
//            next = new TreeMap<>();
//        }
//
//        public Node(){
//            this(false);
//        }
//    }
//
//    //root, size
//    private Node root;
//    private int size;
//
//    //public Trie()
//    public Trie(){
//        root = new Node();
//        size = 0;
//    }
//
//    //int getSize()
//    public int getSize(){
//        return size;
//    }
//
//    public boolean isEmpty(){
//        return size == 0;
//    }
//
//    // 向Trie中添加一个新的单词word void add(String word)
//    public void add(String word){
//        if (word == null || word.length() == 0)
//            throw new IllegalArgumentException("The word is null or empty.");
//
//        Node cur = root;
//        for (int i = 0;i < word.length();i ++){
//            char c = word.charAt(i);
//            if (cur.next.get(c) == null)
//                cur.next.put(c, new Node());
//
//            cur = cur.next.get(c);
//        }
//
//        if (!cur.isWord){
//            cur.isWord = true;
//            size ++;
//        }
//    }
//
//    // 查询单词word是否在Trie中 boolean contains(String word)
//    public boolean contains(String word){
//        if (word == null || word.length() == 0)
//            throw new IllegalArgumentException("The word is null or empty.");
//
//        Node cur = root;
//        for (int i = 0;i < word.length();i ++){
//            char c = word.charAt(i);
//            if (cur.next.get(c) == null)
//                return false;
//
//            cur = cur.next.get(c);
//        }
//
//        return cur.isWord;
//    }
//
//    // 查询是否在Trie中有单词以prefix为前缀
//    public boolean isPrefix(String prefix){
//        if (prefix == null || prefix.length() == 0)
//            throw new IllegalArgumentException("The word is null or empty.");
//
//        Node cur = root;
//        for (int i = 0;i < prefix.length();i ++){
//            char c = prefix.charAt(i);
//            if (cur.next.get(c) == null)
//                return false;
//
//            cur = cur.next.get(c);
//        }
//
//        return true;
//    }
//
//}
