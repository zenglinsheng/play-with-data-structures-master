package LinkedList.AddElementsInLinkedList02;

public class LinkedList<E> {

    private class Node{
        private E e;
        private Node next;

        public Node(E e, Node next){
            this.e = e;
            this.next = next;
        }

        public Node(E e){
            this(e, null);
        }

        public Node(){
            this(null, null);
        }
    }

    //定义Node变量和size
    private Node head;
    private int size;

    //LinkedList()
    public LinkedList(){
        head = null;
        size = 0;
    }

    // 获取链表中的元素个数 int getSize()
    public int getSize(){
        return size;
    }

    //boolean isEmpty()
    public boolean isEmpty(){
        return size == 0;
    }

    //void addFirst(E e)
    public void addFirst(E e){
        /*
        Node node = new Node();
        node.next = head;
        node.e = e;
        head = node
        */
        head = new Node(e, head);
        size ++;
    }

    //void add(int index, E e)
    public void add(int index, E e){
        if (index < 0||index > size){
            throw new IllegalArgumentException("illegal index");
        }

        if (index == 0){
            addFirst(e);
        }else {
            for (int i = 0; i < index - 1; i++) {
                head = head.next;
            }
            /*Node node = new Node();
              node.e = e;
              node.next = head.next;
             head.next = node;*/
            head.next = new Node(e, head.next);
            size ++;
        }

    }

    //void addLast(E e)
    public void addLast(E e){
        add(size, e);
    }

}
