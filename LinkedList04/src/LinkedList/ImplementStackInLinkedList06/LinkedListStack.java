package LinkedList.ImplementStackInLinkedList06;

import LinkedList.DummyHeadInLinkedList03.LinkedList;
import LinkedList.common.Stack;

public class LinkedListStack<E> implements Stack<E> {

    //创建LinkedList成员变量list
    private LinkedList list;

    //LinkedListStack()
    public LinkedListStack(){
        list = new LinkedList();
    }

    @Override
    public int getSize() {
        return list.getSize();
    }

    @Override
    public boolean isEmpty() {
        return list.isEmpty();
    }

    @Override
    public void push(E e) {
        list.addFirst(e);
    }

    @Override
    public E pop() {
        return (E) list.removeFirst();
    }

    @Override
    public E peek() {
        return (E) list.getFirst();
    }

    @Override
    public String toString() {
        return list.toString();
    }

    public static void main(String[] args) {

        LinkedListStack<Integer> stack = new LinkedListStack<>();

        for(int i = 0 ; i < 5 ; i ++){
            stack.push(i);
            System.out.println(stack);
        }

        stack.pop();
        System.out.println(stack);
    }

}
