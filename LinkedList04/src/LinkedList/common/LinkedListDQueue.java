package LinkedList.common;

public class LinkedListDQueue<E> implements Queue<E>{

    private LinkedListD list;

    public LinkedListDQueue(){
        list = new LinkedListD(-1);
    }


    @Override
    public int getSize() {
        return list.getSize();
    }

    @Override
    public boolean isEmpty() {
        return list.isEmpty();
    }

    @Override
    public void enqueue(E e) {
        list.addFirst(e);
    }

    @Override
    public E dequeue() {
        return (E) list.removeLast();
    }

    @Override
    public E getFront() {
        return (E) list.getFirst();
    }

    @Override
    public String toString() {
        return list.toString();
    }

    public static void main(String[] args){

        LinkedListDQueue<Integer> queue = new LinkedListDQueue<>();
        for(int i = 0 ; i < 10 ; i ++){
            queue.enqueue(i);
            System.out.println(queue);
        }
        for(int i = 0 ; i < 10 ; i ++){
            queue.dequeue();
            System.out.println(queue);
        }
    }

}
