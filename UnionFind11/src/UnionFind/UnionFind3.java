package UnionFind;

public class UnionFind3 implements UF {

    private int[] rank;
    private int[] parent;

    public UnionFind3(int size){
        rank = new int[size];
        parent = new int[size];

        for (int i = 0;i < size;i ++) {
            parent[i] = i;
            rank[i] = i;
        }
    }

    @Override
    public int getSize() {
        return parent.length;
    }

    private int find(int p){
        if (p < 0 || p > getSize())
            throw new IllegalArgumentException("The index is illegal.");

        if (p != parent[p])
            parent[p] = find(parent[p]);
        return parent[p];
    }

    @Override
    public boolean isConnected(int p, int q) {
        if (p < 0 || p > getSize() || q < 0 || q > getSize())
            throw new IllegalArgumentException("The index is illegal.");

        return find(p) == find(q);
    }

    @Override
    public void unionElements(int p, int q) {
        if (p < 0 || p > getSize() || q < 0 || q > getSize())
            throw new IllegalArgumentException("The index is illegal.");

        int pID = parent[p];
        int qID = parent[q];

        if (pID == qID)
            return;

        if (rank[pID] < rank[qID]){
            parent[pID] = qID;
        }
        else if (rank[pID] > rank[qID]){
            parent[qID] = pID;
        }else {
            parent[qID] = pID;
            rank[pID] ++;
        }

    }
}
