package UnionFind;

public class UnionFind1 implements UF {

    private int[] id;

    public UnionFind1(int size){
        id = new int[size];

        for (int i = 0;i < size;i ++)
            id[i] = i;
    }

    @Override
    public int getSize() {
        return id.length;
    }

    private int find(int p){
        if (p < 0 || p >= getSize())
            throw new IllegalArgumentException("The index is illegal.");

        return id[p];
    }

    @Override
    public boolean isConnected(int p, int q) {
        if (p < 0 || p > getSize() || q < 0 || q > getSize())
            throw new IllegalArgumentException("The index is illegal.");

        return id[p] == id[q];
    }

    @Override
    public void unionElements(int p, int q) {
        if (p < 0 || p > getSize() || q < 0 || q > getSize())
            throw new IllegalArgumentException("The index is illegal.");

        int pID = id[p];
        int qID = id[q];

        if (pID == qID)
            return;
        for (int i = 0;i < getSize();i ++){
            if (id[i] == pID)
                id[i] = qID;
        }


    }
}
