package MapAndBST02;

import SetAndBST01.FileOperation;

import java.util.ArrayList;

public class LinkedListMap<K, V> implements Map<K, V> {

    //Node getNode(K key)
    private class Node{
        private K key;
        private V value;
        private Node next;

        public Node(K key, V value, Node next){
            this.key = key;
            this.value = value;
            this.next = next;
        }

        public Node(K key, V value){
            this(key, value, null);
        }

        public Node(){
            this(null, null, null);
        }

    }

    private Node dummyHead;
    private int size;

    public LinkedListMap(){
        dummyHead = new Node();
        size = 0;
    }

    private Node getNode(K key){
        Node cur = dummyHead.next;
        while (cur != null){
            if (key.equals(cur.key))
                return cur;
            cur = cur.next;
        }
        return null;
    }

    @Override
    public void add(K key, V value) {
        Node node = getNode(key);
        if (node == null) {
            dummyHead.next = new Node(key, value, dummyHead.next);
            size ++;
        }
    }

    @Override
    public V remove(K key) {
        Node node = getNode(key);
        if (node == null){
            throw new IllegalArgumentException("The key is empty");
        }

        Node pre = dummyHead;
        while (pre.next != null){
            if (key.equals(pre.next.key)){
                Node delNode = pre.next;
                pre.next = delNode.next;
                delNode = null;
                size --;
                break;
            }
        }
        return node.value;
    }

    @Override
    public boolean contains(K key) {
        return getNode(key) != null;
    }

    @Override
    public V get(K key) {
        Node node = getNode(key);
        if (node == null)
            throw new IllegalArgumentException("The key is empty");
        return node.value;
    }

    @Override
    public void set(K key, V newValue) {
        Node node = getNode(key);
        if (node == null)
            throw new IllegalArgumentException("The key is empty");
        Node cur = dummyHead.next;
        while (cur != null){
            if (key.equals(cur.key)){
                cur.value = newValue;
                return;
            }
            cur = cur.next;
        }
    }

    @Override
    public int getSize() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    public static void main(String[] args){
        long startTime = System.nanoTime();

        System.out.println("Pride and Prejudice");

        ArrayList<String> words = new ArrayList<>();
        if(FileOperation.readFile("pride-and-prejudice.txt", words)) {
            System.out.println("Total words: " + words.size());

            LinkedListMap<String, Integer> map = new LinkedListMap<>();
            for (String word : words) {
                if (map.contains(word))
                    map.set(word, map.get(word) + 1);
                else
                    map.add(word, 1);
            }

            System.out.println("Total different words: " + map.getSize());
            System.out.println("Frequency of PRIDE: " + map.get("pride"));
            System.out.println("Frequency of PREJUDICE: " + map.get("prejudice"));
        }

        System.out.println();
        long endTime = System.nanoTime();
        System.out.println("Linked List Set: " + (endTime - startTime) / 1000000000.0 + " s");
    }

}
