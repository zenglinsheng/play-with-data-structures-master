package SetAndBST01;

public class LinkedList<E> {

    private class Node{
        private E e;
        private Node next;

        public Node(E e, Node next){
            this.e = e;
            this.next = next;
        }

        public Node(E e){
            this(e, null);
        }

        public Node(){
            this(null, null);
        }
    }

    //定义虚拟头结点dummyHead和size
    private Node dummyHead;
    private int size;

    //LinkedList()
    public LinkedList(){
        dummyHead = new Node();
        size = 0;
    }

    //int getSize()
    public int getSize(){
        return size;
    }

    //boolean isEmpty()
    public boolean isEmpty(){
        return size == 0;
    }

    //void add(int index, E e)
    public void add(int index, E e){
        if (index < 0 || index > size){
            throw new IllegalArgumentException("illeagl index");
        }

        Node prex = dummyHead;
        for (int i = 0;i < index;i ++){
            prex = prex.next;
        }
        prex.next = new Node(e, prex.next);
        size ++;
    }

    //void addFirst(E e)
    public void addFirst(E e){
        add(0, e);
    }

    //void addLast(E e)
    public void addLast(E e){
        add(size, e);
    }

    //E get(int index)
    public E get(int index){
        if (index < 0 || index > size -1){
            throw new IllegalArgumentException("illeagl index");
        }

        Node prex = dummyHead;
        for (int i = 0;i < index;i ++){
            prex = prex.next;
        }
        return prex.next.e;
    }

    //E getFirst()
    public E getFirst(){
        return get(0);
    }

    //E getLast()
    public E getLast(){
        return get(size - 1);
    }

    //void set(int index, E e)
    public void set(int index, E e){
        if (index < 0 || index > size - 1){
            throw new IllegalArgumentException("illeagl index");
        }

        Node prex = dummyHead.next;
        for (int i = 0;i < index;i ++){
            prex = prex.next;
        }
        prex.e = e;

    }

    //boolean contains(E e)
    public boolean contains(E e){
        Node prex = dummyHead.next;
        while (prex != null){
            if (prex.e.equals(e)){
                return true;
            }
            prex = prex.next;
        }
        return false;
    }

    //E remove(int index)
    public E remove(int index){
        if (index < 0 || index > size - 1){
            throw new IllegalArgumentException("illeagl index");
        }

        Node prex = dummyHead;
        for (int i = 0;i < index;i ++){
           prex = prex.next;
        }

        Node ret = prex.next;
        E e = ret.e;
        prex.next = ret.next;
        ret = null;
        size --;
        return e;
    }

    //E removeFirst()
    public E removeFirst(){
        return remove(0);
    }

    //E removeLast()
    public E removeLast(){
        return remove(size - 1);
    }

    //从链表中删除元素e void removeElement(E e)
    public void removeElement(E e){
        Node prex = dummyHead;
        while (prex.next != null){
            if (prex.next.e.equals(e)){
                prex.next = null;
                prex.next = prex.next.next;
                size --;
                break;
            }
            prex = prex.next;
        }

    }

    //toString()


    @Override
    public String toString() {
        StringBuilder res = new StringBuilder();

        Node cur = dummyHead.next;
        while(cur != null){
            res.append(cur.e + "->");
            cur = cur.next;
        }
        res.append("NULL");

        return res.toString();
    }

    public static void main(String[] args) {

        LinkedList<Integer> linkedList = new LinkedList<>();
        for(int i = 0 ; i < 15 ; i ++){
            linkedList.addFirst(i);
            System.out.println(linkedList);
        }

        linkedList.add(2, 666);
        System.out.println(linkedList);

        linkedList.remove(2);
        System.out.println(linkedList);

        linkedList.removeFirst();
        System.out.println(linkedList);

        linkedList.removeLast();
        System.out.println(linkedList);
    }

}
