package SetAndBST01;

import java.util.TreeSet;

public class Solution {

    private static String[] codes = {".-","-...","-.-.","-..",".","..-.","--.","....","..",".---","-.-",".-..","--","-.","---",".--.","--.-",".-.","...","-","..-","...-",".--","-..-","-.--","--.."};

    public int uniqueMorseRepresentationsByBST(String[] words){
        BST<String> bst = new BST();
        for (String word: words){
            StringBuilder sb = new StringBuilder();
            for (int i = 0;i < word.length();i ++)
                sb.append(codes[word.charAt(i) - 'a']);

            bst.add(sb.toString());
        }
        return bst.getSize();
    }

    public int uniqueMorseRepresentationsByLinkenListSet(String[] words){
        LinkedListSet<String> listSet = new LinkedListSet();
        for (String word: words){
            StringBuilder sb = new StringBuilder();
            for (int i = 0;i < word.length();i ++)
                sb.append(codes[word.charAt(i) - 'a']);

            listSet.add(sb.toString());
        }
        return listSet.getSize();
    }

    public int uniqueMorseRepresentationsByTreeMap(String[] words){
        TreeSet<String> treeSet = new TreeSet();
        for (String word: words){
            StringBuilder sb = new StringBuilder();
            for (int i = 0;i < word.length();i ++)
                sb.append(codes[word.charAt(i) - 'a']);

            treeSet.add(sb.toString());
        }
        return treeSet.size();
    }

    public static void main(String[] args){
        String[] words = {"arc", "dde", "wee", "arc", "rrd", "erd"};
        System.out.println((new Solution()).uniqueMorseRepresentationsByBST(words));
        System.out.println((new Solution()).uniqueMorseRepresentationsByLinkenListSet(words));
        System.out.println((new Solution()).uniqueMorseRepresentationsByTreeMap(words));
    }

}
