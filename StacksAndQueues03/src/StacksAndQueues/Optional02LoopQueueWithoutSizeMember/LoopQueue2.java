package StacksAndQueues.Optional02LoopQueueWithoutSizeMember;

import StacksAndQueues.common.Queue;

public class LoopQueue2<E> implements Queue<E> {

    private E[] data;
    private int front, tail;

    public LoopQueue2(int capacity){
        data = (E[]) new Object[capacity];
        front = 0;
        tail = 0;
    }

    public LoopQueue2(){
        this(10);
    }

    public int getCapacity(){
        return data.length;
    }

    @Override
    public int getSize() {
        if (data[front] == null){
            return 0;
        }else if (front <= tail){
            return tail - front + 1;
        }else {
            return data.length - (front - tail - 1);
        }
    }

    @Override
    public boolean isEmpty() {
        return data[front] == null;
    }

    @Override
    public void enqueue(E e) {
        if (data[front] == null){
            data[tail] = e;
        }else {
            tail = (tail + 1) % data.length;
            data[tail] = e;
        }
        if (front-tail == 1 || (tail - front + 1) == data.length){
            resize(getCapacity() * 2);
        }
    }

    @Override
    public E dequeue() {
        if (data[front] == null){
            throw new IllegalArgumentException("队列为空");
        }
        E ret = data[front];
        data[front] = null;
        front = (front + 1) % data.length;

        if (((tail - front + 1) == getCapacity()/4 || (front - tail - 1) == getCapacity() * 3/4)
                && getCapacity()/2 != 0){
            resize(getCapacity()/2);
        }
        return ret;
    }

    @Override
    public E getFront() {
        if (data[front] == null){
            throw new IllegalArgumentException("队列为空");
        }
        return data[front];
    }

    public void resize(int newCapacity){
        E[] newData = (E[]) new Object[newCapacity];
        for (int i = 0;i < getSize();i ++){
            newData[i] = data[(front + i) % data.length];
        }
        tail = getSize() - 1; //必须在front = 0之前执行
        front = 0;
        data = newData;
    }

    //这里的遍历容易出错
    @Override
    public String toString() {
        StringBuilder res = new StringBuilder();
        res.append(String.format("Queue: size = %d , capacity = %d\n", getSize(), getCapacity()));
        res.append("front [");

        // 注意，我们的循环遍历打印队列的逻辑也有相应的更改 :-)
        for(int i = 0; i < getSize(); i ++){
            res.append(data[(front + i) % data.length]);
            if((i + front) % data.length != tail)
                res.append(",");
        }
        res.append("] tail");
        return res.toString();
    }

    public static void main(String[] args){

        LoopQueue2<Integer> queue = new LoopQueue2<>();
        for(int i = 0 ; i < 10 ; i ++){
            queue.enqueue(i);
            System.out.println(queue);

            if(i % 3 == 2){
                queue.dequeue();
                System.out.println(queue);
            }
        }
    }

}
