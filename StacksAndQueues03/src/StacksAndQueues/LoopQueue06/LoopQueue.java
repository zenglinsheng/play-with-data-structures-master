package StacksAndQueues.LoopQueue06;

import StacksAndQueues.common.Queue;

public class LoopQueue<E> implements Queue<E> {

    //E[] data, front, tail, size
    private E[] data;
    private int front, tail;
    private int size;

    //LoopQueue(int capacity)
    public LoopQueue(int capacity){
        data = (E[]) new Object[capacity + 1];
        front = 0;
        tail = 0;
        size = 0;
    }

    //LoopQueue()
    public LoopQueue(){
        this(10);
    }

    //int getCapacity()
    public int getCapacity(){
        return data.length - 1;
    }

    @Override
    public int getSize() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public void enqueue(E e) {
        if (size == 0){
            data[front] = e;
        }else {
            data[tail] = e;
        }
        tail = (tail + 1) % data.length;
        size ++;
        if (size == getCapacity()){
            resize(getCapacity() * 2);
        }
    }

    @Override
    public E dequeue() {
        if (size <= 0){
            throw new IllegalArgumentException("队列为空");
        }
        E ret = data[front];
        data[front] = null;
        front = (front + 1) % data.length;
        size --;

        if (size == getCapacity()/4 && getCapacity()/2 != 0){
            resize(getCapacity()/2);
        }
        return ret;
    }

    @Override
    public E getFront() {
        if (size <= 0){
            throw new IllegalArgumentException("队列为空");
        }
        return data[front];
    }

    //void resize(int newCapacity)  扩容容易出错（未维护newData地front和tail）
    public void resize(int newCapacity){
        E[] newData = (E[]) new Object[newCapacity + 1];
        for (int i = 0;i < size;i ++){
            newData[i] = data[(front + i) % data.length];
        }
        front = 0;
        tail = size;
        data = newData;
    }

    @Override
    public String toString() {
        StringBuilder res = new StringBuilder();
        res.append(String.format("Queue: size = %d , capacity = %d\n", size, getCapacity()));
        res.append("front [");
        for(int i = front ; i != tail ; i = (i + 1) % data.length){
            res.append(data[i]);
            if((i + 1) % data.length != tail)
                res.append(", ");
        }
        res.append("] tail");
        return res.toString();
    }

    public static void main(String[] args){
        LoopQueue queue = new LoopQueue();
        for (int i = 0;i < 5;i ++)
            queue.enqueue(i + 1);
        //System.out.println(queue);

        queue.dequeue();
        queue.dequeue();
        //System.out.println(queue);

        for (int i = 5;i < 12;i ++) {
            queue.enqueue(i + 1);
            System.out.println(queue);
        }
        //System.out.println(queue);

        System.out.println(queue.getFront());
    }

}
