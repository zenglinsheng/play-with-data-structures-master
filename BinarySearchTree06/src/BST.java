import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class BST<E extends Comparable<E>> {

    private class Node{
        private E e;
        private Node left, right;

        public Node(E e, Node left, Node right){
            this.e = e;
            this.left = left;
            this.right = right;
        }

        public Node(E e){
            this(e, null, null);
        }
    }

    private Node root;
    private int size;

    public BST(Node root){
        this.root = root;
        size = 0;
    }

    public BST(){
        this(null);
    }

    public int getSize(){
        return size;
    }

    public boolean isEmpty(){
        return size == 0;
    }

    public void add(E e){
        root = add(root, e);
    }

    private Node add(Node node, E e){
        if (node == null){
            size ++;
            return new Node(e);
        }

        if (e.compareTo(node.e) < 0){
            node.left = add(node.left, e);
        }else if (e.compareTo(node.e) > 0){
            node.right = add(node.right, e);
        }
        return node;
    }

    public boolean contains(E e){
        return contains(root, e);
    }

    private boolean contains(Node node, E e){
        if (node == null) return false;

        if (e.compareTo(node.e) == 0) return true;
        if (e.compareTo(node.e) < 0) return contains(node.left, e);
        else return contains(node.right, e);
    }

    public void preOrder(){
        preOrder(root);
    }

    private void preOrder(Node node){
        if (node == null) return;

        System.out.print(node.e + " ");
        preOrder(node.left);
        preOrder(node.right);
    }

    public void inOrder(){
        inOrder(root);
    }

    private void inOrder(Node node){
        if (node == null) return;

        inOrder(node.left);
        System.out.print(node.e + " ");
        inOrder(node.right);
    }

    public void postOrder(){
        postOrder(root);
    }

    private void postOrder(Node node){
        if (node == null) return;

        postOrder(node.left);
        postOrder(node.right);
        System.out.print(node.e + " ");
    }

    // 二分搜索树的非递归前序遍历 void preOrderNR()
    public void preOrderNR(){
        if (root == null) return;

        Stack<Node> stack = new Stack<>();
        stack.push(root);
        while (!stack.empty()){
            Node node = stack.pop();
            System.out.print(node.e + " ");

            if (node.right != null) stack.push(node.right);
            if (node.left != null) stack.push(node.left);
        }
    }

    // 二分搜索树的层序遍历 void levelOrder()
    public void levelOrder(){
        if (root == null) return;

        Queue<Node> queue = new LinkedList<>();
        queue.add(root);
        while (!queue.isEmpty()){
            Node node = queue.remove();
            System.out.print(node.e + " ");

            if (node.left != null) queue.add(node.left);
            if (node.right != null) queue.add(node.right);
        }
    }

    // 寻找二分搜索树的最小元素 E minimum()
    public E minimum(){
        if (getSize() == 0) {
            throw new IllegalArgumentException("The tree is null");
        }
        return minimun(root).e;
    }

    // 返回以node为根的二分搜索树的最小值所在的节点 Node minimum(Node node)
    private Node minimun(Node node){
        if (node.left == null) return node;
        return minimun(node.left);
    }

    // 寻找二分搜索树的最大元素 E maximum()
    public E maximum(){
        if (getSize() == 0) {
            throw new IllegalArgumentException("The tree is null");
        }
        return maximum(root).e;
    }

    // 返回以node为根的二分搜索树的最大值所在的节点 Node maximum(Node node)
    private Node maximum(Node node){
        if (node.right == null) return node;
        return maximum(node.right);
    }

    // 从二分搜索树中删除最小值所在节点, 返回最小值 E removeMin()
    public E removeMin(){
        E e = minimum();
        root = removeMin(root);
        return e;
    }

    // 删除掉以node为根的二分搜索树中的最小节点
    // 返回删除节点后新的二分搜索树的根 Node removeMin(Node node)
    private Node removeMin(Node node){

        if (node.left == null){
            Node rightNode = node.right;
            node.right = null;
            size -- ;
            return rightNode;
        }
        node.left = removeMin(node.left);
        return node;
    }

    // 从二分搜索树中删除最大值所在节点 E removeMax()
    public E removeMax(){
        E e = maximum();
        root = removeMax(root);
        return e;
    }

    // 删除掉以node为根的二分搜索树中的最大节点
    // 返回删除节点后新的二分搜索树的根 Node removeMax(Node node)
    private Node removeMax(Node node){

        if (node.right == null){
            Node leftNode = node.left;
            node.left = null;
            size --;
            return leftNode;
        }
        node.right = removeMax(node.right);
        return node;
    }

    // 从二分搜索树中删除元素为e的节点 void remove(E e)
    public void remove(E e){
        root = remove(root, e);
    }

    // 删除掉以node为根的二分搜索树中值为e的节点, 递归算法
    // 返回删除节点后新的二分搜索树的根 Node remove(Node node, E e)
    private Node remove(Node node, E e){
        if (root == null) return null;

        if (e.compareTo(node.e) < 0){
           node.left = remove(node.left, e);
           return node;
        }else if (e.compareTo(node.e) > 0){
            node.right = remove(node.right, e);
            return node;
        }else { //e.compareTo(node.e) == 0

            if (node.left == null) {
                Node rightNode = node.right;
                node.right = null;
                size --;
                return rightNode;
            }else if (node.right == null){
                Node leftNode = node.left;
                node.left = null;
                size --;
                return leftNode;
            }
            Node successor = minimun(node.right);
            /*这两步的顺序不能交换，因为此时successor指向了node.right,
            如果先执行successor.left = node.left;就会导致node.right = node.left,
            所以在removeMin(node.right)中传入的参数是修改后的*/
            successor.right = removeMin(node.right);
            successor.left = node.left;

            node.left = node.right = null;
            return successor;
        }
    }

    public E floor(E e){

        if( getSize() == 0 || e.compareTo(minimum()) < 0 )
            return null;

        Node floorNode = floor(root, e);
        return floorNode == null ? null:floorNode.e;
    }

    private Node floor(Node node, E e){

        if(node == null) return null;

        if(node.e.compareTo(e) == 0){
            if (node.left == null) return null;
            return maximum(node.left);
        }

        //如果该node比key要大的话
        if(node.e.compareTo(e) > 0){
            if (node.left == null) return node;
            return floor(node.left, e);
        }

        //如果node比key小，可能是，也能是不是
        Node tempNode = floor(node.right, e);
        if(tempNode != null)  return tempNode;
        return node;   //相当于 tempNode == null
    }

    public E ceil(E e){

        if( getSize() == 0 || e.compareTo(maximum()) > 0 )
            return null;

        Node ceilNode = ceil(root, e);
        return ceilNode == null ? null:ceilNode.e;
    }

    private Node ceil(Node node, E e){

        if(node == null) return null;

        if(node.e.compareTo(e) == 0){
            if (node.right == null) return null;
            return minimun(node.right);
        }

        if(node.e.compareTo(e) < 0){
            if (node.right == null) return node;
            return ceil(node.right, e);
        }

        Node tempNode = ceil(node.left, e);
        if(tempNode != null)  return tempNode;
        return node;   //相当于 tempNode == null
    }

    public static void main(String[] args){
        BST<Integer> bst = new BST<>();
        int[] nums = {5, 3, 6, 8, 4, 2};
        for(int num: nums)
            bst.add(num);
        bst.inOrder();
        System.out.println();
        System.out.println(bst.ceil(7));
    }

}
