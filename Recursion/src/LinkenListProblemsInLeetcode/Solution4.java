package LinkenListProblemsInLeetcode;

public class Solution4 {

    public ListNode removeElements(ListNode head, int value){
        if (head == null) return null;
        head.next = removeElements(head.next, value);
        return head.value == value ? head.next:head;
    }


    public static void main(String[] args) {

        int[] nums = {1, 2, 6, 3, 4, 5, 6};
        ListNode head = new ListNode(nums);
        System.out.println(head);

        ListNode res = (new Solution4()).removeElements(head, 6);
        System.out.println(res);
    }

}
