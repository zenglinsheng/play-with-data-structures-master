package LinkenListProblemsInLeetcode;

public class Solution {

    public ListNode removeElements(ListNode head, int value){

        while (head != null && head.value == value){
            ListNode del = head;
            head = del.next;
            del.next = null;
        }

        if (head == null){
            return null;
        }

        ListNode pre = head;
        while (pre.next != null){
            if (pre.next.value == value){
                ListNode del = pre.next;
                pre.next = del.next;
                del.next = null;
            }else {
                pre = pre.next;
            }
        }
        return head;
    }

    public static void main(String[] args) {

        int[] nums = {1, 2, 6, 3, 4, 5, 6};
        ListNode head = new ListNode(nums);
        System.out.println(head);

        ListNode res = (new Solution()).removeElements(head, 6);
        System.out.println(res);
    }

}
