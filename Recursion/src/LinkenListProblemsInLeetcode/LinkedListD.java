package LinkenListProblemsInLeetcode;

import javax.naming.NoInitialContextException;

/**
 * 双向链表
 */

public class LinkedListD<E> {

    private class Node{
        public E e;
        public Node next;
        public Node prev;

        public Node(E e, Node next, Node prev){
            this.e = e;
            this.next = next;
            this.prev = prev;
        }

        public Node(E e){
            this(e, null, null);
        }

        public Node(){
            this(null, null, null);
        }

        @Override
        public String toString() {
            return e.toString();
        }
    }

    private Node dummyHead;
    private Node tail;
    private int size;

    public LinkedListD(E e){
        dummyHead = new Node(e);
        tail = null;
        size = 0;
    }

    public int getSize(){
        return size;
    }

    public boolean isEmpty(){
        return size == 0;
    }

    public void add(int index, E e){
        if (index < 0 || index > getSize()){
            throw new IllegalArgumentException("index is illegal");
        }

        Node node = new Node(e);
        if (tail == null){
            node.prev = dummyHead;
            dummyHead.next = node;
            tail = node;
            size ++;
            return;
        }

        if (index == getSize()){
            tail.next = node;
            node.prev = tail;
            tail = node;
            size ++;
            return;
        }

        Node prev = dummyHead;
        for (int i = 0;i < index;i ++){
            prev = prev.next;
        }
        node.next = prev.next;
        prev.next.prev = node;
        prev.next = node;
        node.prev = prev;
        size ++;
    }

    public void addFirst(E e){
        add(0, e);
    }

    public void addLast(E e){
        add(getSize(), e);
    }

    public E get(int index){
        if (index < 0 || index >= getSize()){
            throw new IllegalArgumentException("The index is illegal");
        }

        Node prev = dummyHead.next;
        for (int i = 0;i < index;i ++){
            prev = prev.next;
        }
        return prev.e;
    }

    public E getFirst(){
        return get(0);
    }

    public E getLast(){
        return get(getSize() - 1);
    }

    public void set(int index, E e){
        if (index < 0 || index >= getSize()){
            throw new IllegalArgumentException("The index is illegal");
        }

        Node prev = dummyHead.next;
        for (int i = 0;i < index;i ++){
            prev = prev.next;
        }
        prev.e = e;
    }

    public void setFirst(E e){
        set(0, e);
    }

    public void setLast(E e){
        set(getSize() - 1, e);
    }

    public boolean contains(E e){
        Node prev = dummyHead.next;
        while (prev != null){
            if (prev.e.equals(e)){
                return true;
            }
            prev = prev.next;
        }
        return false;
    }

    public E remove(int index){
        if (index < 0 || index >= getSize()){
            throw new IllegalArgumentException("The index is illegal");
        }

        int mid = getSize()/2;
        if (index < mid){
            Node cur = dummyHead;
            for (int i = 0;i < index;i ++){
                cur = cur.next;
            }
            Node del = cur.next;
            cur.next = del.next;
            del.next.prev = del.prev;
            del.next = null;
            del.prev = null;
            size -- ;
            return del.e;
        }else {
            Node cur = tail;
            for (int i = getSize() - 1;i > index;i --){
                cur = cur.prev;
            }
            tail = cur.prev;
            cur.prev.next = null;
            cur.next = null;
            cur.prev = null;
            size --;
            return cur.e;
        }
    }

    public E removeFirst(){
        return remove(0);
    }

    public E removeLast(){
        return remove(getSize() - 1);
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        Node prev = dummyHead;
        while (prev.next != null){
            sb.append(prev.next.e + "->");
            prev = prev.next;
        }
        sb.append("Null");
        return sb.toString();
    }

    public static void main(String[] args){
        LinkedListD<Integer> list = new LinkedListD<>(-1);
        for (int i = 0; i < 10;i ++){
            list.add(i, i);
        }
        list.add(2, 99);
        list.addFirst( 11);
        list.addLast( 100);
        System.out.println(list);
        /*System.out.println("get first: " + list.getFirst());
        System.out.println("get last: " + list.getLast());
        System.out.println("get index 3: " + list.get(3));*/

        /*list.setFirst(1000);
        list.setLast(999);
        list.set(3, 888);
        System.out.println(list);*/

        /*System.out.println(list.contains(99));
        System.out.println(list.contains(12345));
        System.out.println(list.contains(9));*/
        System.out.println(list.remove(0) + "   size:  " + list.getSize());
        System.out.println(list);
        System.out.println(list.remove(list.getSize() - 1)+ "   size:  " + list.getSize());
        System.out.println(list);
        System.out.println(list.remove(3)+ "   size:  " + list.getSize());
        System.out.println(list);

    }
}

