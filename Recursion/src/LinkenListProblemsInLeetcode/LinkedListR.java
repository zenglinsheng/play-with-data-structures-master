package LinkenListProblemsInLeetcode;

import javafx.util.Pair;

//递归实现的LinkedList
//类名称中LinkedListR里的R，是Recursion的意思，表示递归实现：）
public class LinkedListR<E> {

    private class Node{
        private E e;
        private Node next;

        public Node(E e, Node next){
            this.e = e;
            this.next = next;
        }
        public Node(E e){
            this(e, null);
        }
        public Node(){
            this(null, null);
        }
    }

    private Node head;
    private int size;

    // 获取链表中的元素个数 int getSize()
    public int getSize(){
        return size;
    }

    // 返回链表是否为空 boolean isEmpty()
    public boolean isEmpty(){
        return size == 0;
    }

    // 在链表的index(0-based)位置添加新的元素e void add(int index, E e)
    public void add(int index, E e){
        if (index < 0 || index > getSize()){
            throw new IllegalArgumentException("The index is Illeagl");
        }
        head = add(head, index, e);
        size ++;
    }

    // 在以node为头结点的链表的index位置插入元素e，递归算法 Node add(Node node, int index, E e)
    private Node add(Node node, int index, E e){
        if (index == 0){
            return new Node(e, node);
        }
        node.next = add(node, index - 1, e);
        return node;
    }

    // 在链表头添加新的元素e void addFirst(E e)
    public void addFirst(E e){
        add(0, e);
    }

    // 在链表末尾添加新的元素e void addLast(E e)
    public void addLast(E e){
        add(getSize(), e);
    }

    // 获得链表的第index(0-based)个位置的元素 E get(int index)
    public E get(int index){
        if (index < 0 || index >= getSize()){
            throw new IllegalArgumentException("The index is Illeagl");
        }
        return get(head, index);
    }

    // 在以node为头结点的链表中，找到第index个元素，递归算法 E get(Node node, int index)
    private E get(Node node, int index){
        if (index == 0){
            return node.e;
        }
        return get(node.next, index - 1);
    }

    // 获得链表的第一个元素 E getFirst()
    public E getFirst(){
        return get(0);
    }

    // 获得链表的最后一个元素 E getLast()
    public E getLast(){
        return get(getSize() - 1);
    }

    // 修改链表的第index(0-based)个位置的元素为e void set(int index, E e)
    public void set(int index, E e){
        set(head, index, e);
    }

    // 修改以node为头结点的链表中，第index(0-based)个位置的元素为e，递归算法 void set(Node node, int index, E e)
    private void set(Node node, int index, E e){
        if (index == 0){
            node.e = e;
            return;
        }
        set(node.next, index -1, e);
    }

    // 查找链表中是否有元素e boolean contains(E e)
    public boolean contains(E e){
        return contains(head, e);
    }

    // 在以node为头结点的链表中，查找是否存在元素e，递归算法 boolean contains(Node node, E e)
    private boolean contains(Node node, E e){
        if (node.e.equals(e)){
            return true;
        }
        return contains(node.next, e);
    }

    // 从链表中删除index(0-based)位置的元素, 返回删除的元素 E remove(int index)
    public E remove(int index){
        if (index < 0 || index >= getSize()){
            throw new IllegalArgumentException("The index is Illeagl");
        }
        Pair<Node, E> pair = remove(head, index);
        size --;
        head = pair.getKey();
        return pair.getValue();
    }

    // 从以node为头结点的链表中，删除第index位置的元素，递归算法 Pair<Node, E> remove(Node node, int index)
    // 返回值包含两个元素，删除后的链表头结点和删除的值：）
    private Pair<Node, E> remove (Node node, int index){
        if (index == 0){
            return new Pair<>(node.next, node.e);
        }
        Pair<Node,E> pair = remove(node.next, index - 1);
        node.next = pair.getKey();
        return new Pair<>(node, pair.getValue());
    }

    // 从链表中删除第一个元素, 返回删除的元素 E removeFirst()
    public E removeFirst(){
        return remove(0);
    }

    // 从链表中删除最后一个元素, 返回删除的元素 E removeLast()
    public E removeLast(){
        return remove(getSize() - 1);
    }

    // 从链表中删除元素e void removeElement(E e)
    public void removeElement(E e){
        removeElement(head, e);
        size --;
    }

    // 从以node为头结点的链表中，删除元素e，递归算法 Node removeElement(Node node, E e)
    private Node removeElement(Node node, E e){
        if (node.e.equals(e)){
            return node.next;
        }
        return removeElement(node.next, e);
    }

    //toString()
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        Node pre = head;
        while (pre != null){
            sb.append(pre.e + "->");
            pre = pre.next;
        }
        sb.append("Null");
        return sb.toString();
    }

    private interface Stack<E> {

        int getSize();
        boolean isEmpty();
        void push(E e);
        E pop();
        E peek();
    }

    private class LinkedListStack<E> implements Stack<E> {

        private LinkedListR<E> list;

        public LinkedListStack(){
            list = new LinkedListR<>();
        }

        @Override
        public int getSize() {
            return list.getSize();
        }

        @Override
        public boolean isEmpty() {
            return list.isEmpty();
        }

        @Override
        public void push(E e) {
            list.addFirst(e);
        }

        @Override
        public E pop() {
            return list.removeFirst();
        }

        @Override
        public E peek() {
            return list.getFirst();
        }

        @Override
        public String toString(){
            StringBuilder res = new StringBuilder();
            res.append("Stack: top ");
            res.append(list);
            return res.toString();
        }

    }

    public static void main(String[] args) {

        /*LinkedListR<Integer> list = new LinkedListR<>();
        for(int i = 0 ; i < 10 ; i ++)
            list.addFirst(i);
        System.out.println(list);

        while(!list.isEmpty())
            System.out.println("removed " + list.removeLast());*/

        String str = "(){}[)";
        LinkedListR<Object> list = new LinkedListR<>();
        System.out.println(list.stringMatching(str));
    }

    private boolean stringMatching(String str){
        if (str == null){
            throw new IllegalArgumentException("The string is null");
        }

        Stack<Character> stack = new LinkedListStack<>();
        for (int i = 0; i < str.length();i ++){
            char c = str.charAt(i);
            if (c == '(' || c == '[' || c == '{'){
                stack.push(c);
            }else {
                if (stack.isEmpty()) return false;
                Character p = stack.pop();
                if (c == ')' && p != '(') return false;
                if (c == ']' && p != '[') return false;
                if (c == '}' && p != '{') return false;
            }
        }

        return stack.isEmpty();
    }



}
