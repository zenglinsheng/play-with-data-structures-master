package LinkenListProblemsInLeetcode;

public class ListNode {

    public int value;
    public ListNode next;

    public ListNode(int value){
        this.value = value;
        this.next = null;
    }

    public ListNode(int[] arr){
        if (arr == null || arr.length ==0){
            throw new IllegalArgumentException("The arr is Illegal");
        }

        this.value = arr[0];
        ListNode cur = this;
        for (int i = 1;i < arr.length;i ++){
            cur.next = new ListNode(arr[i]);
            cur = cur.next;
        }
    }

    @Override
    public String toString() {
        ListNode head = this;
        StringBuffer sb = new StringBuffer();
        while (head != null){
            sb.append(head.value + "->");
            head = head.next;
        }
        sb.append("Null");
        return sb.toString();
    }

    public static void main(String[] args){
        int[] arr = {1,4,6,2,88,55,12};
        ListNode head = new ListNode(arr);
        System.out.println(head);
    }
}
