package Heap;

public class MaxHeap<E extends Comparable<E>> {

    private Array<E> data;

    // MaxHeap(int capacity)
    public MaxHeap(int capacity){
        data = new Array<>(capacity);
    }

    // MaxHeap()
    public MaxHeap(){
        data = new Array<>();
    }

    // public MaxHeap(E[] arr)
    public MaxHeap(E[] arr){
        data = new Array<>(arr);
        for (int i = parent(getSize() - 1);i >= 0;i --)
            siftDown(i);
    }

    // 返回堆中的元素个数 int getSize()
    public int getSize(){
        return data.getSize();
    }

    // 返回一个布尔值, 表示堆中是否为空 boolean isEmpty()
    public boolean isEmpty(){
        return data.isEmpty();
    }

    /*         0
             /   \
            1     2
           / \   / \
          3   4 5   6
    */
    // 返回完全二叉树的数组表示中，一个索引所表示的元素的父亲节点的索引 int parent(int index)
    private int parent(int index){
        if (index == 0) throw new IllegalArgumentException("The index not have parent.");
        return (index - 1)/2;
    }

    // 返回完全二叉树的数组表示中，一个索引所表示的元素的左孩子节点的索引 int leftChild(int index)
    private int leftChild(int index){
        return index * 2 + 1;
    }

    // 返回完全二叉树的数组表示中，一个索引所表示的元素的右孩子节点的索引 int rightChild(int index)
    private int rightChild(int index){
        return index * 2 + 2;
    }

    // 向堆中添加元素 void add(E e)
    public void add(E e){
        data.addLast(e);
        siftUp(getSize() - 1);
    }

    // private void siftUp(int k)
    private void siftUp(int k){
        while (k > 0 && data.get(k).compareTo(data.get(parent(k))) > 0){
            data.swap(k, parent(k));
            k = parent(k);
        }
    }

    // 看堆中的最大元素 E findMax()
    public E findMax(){
        if (isEmpty())
            throw new IllegalArgumentException("The heap is empty.");
        return data.get(0);
    }

    // 取出堆中最大元素 E extractMax()
    public E extractMax(){
        E max = findMax();
        if (max == null)
            throw new IllegalArgumentException("The heap is empty.");

        data.swap(0 , getSize() - 1);
        data.removeLast();
        siftDown(0);

        return max;
    }

    // private void siftDown(int k)
    private void siftDown(int k){
        int j = leftChild(k);
        while (j < getSize()){
            if (j + 1 < getSize() && data.get(j + 1).compareTo(data.get(j)) > 0){
               j = j + 1;
            }

            if (data.get(k).compareTo(data.get(j)) >= 0)
                break;

            data.swap(k, j);
            k = j;
            j = leftChild(k);
        }
    }

    // 取出堆中的最大元素，并且替换成元素e E replace(E e)
    public E repalce(E e){
        E max = findMax();
        data.set(0, e);
        siftDown(0);
        return max;
    }

    public static void main(String[] args){
        Integer[] arr = {1,4,5,6,2,100,20,88};
        MaxHeap<Integer> heap = new MaxHeap<>(arr);
        System.out.println(heap.findMax());
    }


}
