package Heap;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

public class Solution {

    private class Freq implements Comparable<Freq>{

        public int e, freq;

        public Freq(int e, int freq){
            this.e = e;
            this.freq = freq;
        }

        @Override
        public int compareTo(Freq o) {
            return o.freq - freq;
        }
    }

    public List<Integer> topKFrequentByPriotityQueue(int[] nums, int k){
        TreeMap<Integer, Integer> treeMap = new TreeMap<>();
        for (Integer num: nums)
            if (!treeMap.containsKey(num))
                treeMap.put(num, 1);
            else
                treeMap.put(num, treeMap.get(num) + 1);

        PriorityQueue<Freq> pq = new PriorityQueue<>();
        for (Integer key: treeMap.keySet()){
            Freq freq = new Freq(key, treeMap.get(key));
            if (pq.getSize() < k)
                pq.enqueue(freq);
            else if (freq.freq > pq.getFront().freq) {
                pq.dequeue();
                pq.enqueue(freq);
            }
        }

        ArrayList<Integer> list = new ArrayList<>();
        while (!pq.isEmpty())
            list.add(pq.dequeue().e);
        return list;
    }

    private static void printList(List<Integer> nums){
        for(Integer num: nums)
            System.out.print(num + " ");
        System.out.println();
    }

    public static void main(String[] args) {

        int[] nums = {1, 1, 1, 2, 2, 3, 6, 6, 6};
        int k = 2;
        printList((new Solution()).topKFrequentByPriotityQueue(nums, k));
    }

}
